const faker = require('faker');

// BEGIN
let result = {};

beforeEach(() => {
  Date.now = jest.fn(() => 1572393600000); // 2019-10-30T00:00Z0 (GMT)
  result = faker.helpers.createTransaction();
});

test('objects should match', () => {
  expect(result).toStrictEqual({
    'amount': expect.any(String),
    'date': expect.any(Date),
    'business': expect.any(String),
    'name': expect.any(String),
    'type': expect.any(String),
    'account': expect.any(String),
  });
});

test('objects should NOT match', () => {
  
  expect(result).not.toStrictEqual({
    'birthday': expect.any(String),
    'adress': expect.any(String),
    'amount': expect.any(String),
    'date': expect.any(Date),
    'business': expect.any(String),
    'name': expect.any(String),
    'type': expect.any(String),
    'account': expect.any(String),
  });
  expect(result).not.toStrictEqual({
    'amount': expect.any(String),
    'type': expect.any(String),
    'account': expect.any(String),
  });

});

test('objects should not have to be equal', () => {
  const object = faker.helpers.createTransaction();

  expect(result).not.toStrictEqual(object);
})
// END
