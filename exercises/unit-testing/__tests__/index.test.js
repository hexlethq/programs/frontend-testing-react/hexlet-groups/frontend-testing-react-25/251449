test('main', () => {
  const src = { k: 'v', b: 'b' };
  const target = { k: 'v2', a: 'a' };
  const result = Object.assign(target, src);

  expect.hasAssertions();

  // BEGIN
  expect(result).toEqual({ k: 'v', a: 'a', b: 'b' });
  expect(Object.assign({}, {})).toEqual({});
  expect(Object.assign({}, { a: 'a' })).toEqual({ a: 'a' });
});

test('should not modify non-target objects', () => {
  const src = { k: 'v', b: 'b' };
  const target = { k: 'v2', a: 'a' };
  const result = Object.assign(target, src);

  expect.hasAssertions();

  expect(result).toEqual(target);
  expect(src).toEqual({ k: 'v', b: 'b' });
});

test('should modify target object', () => {
  const obj1 = { a: 'a' }; 
  const obj3 = { k: 'k' };
  const result = Object.assign(obj1, obj3);

  expect(obj1).toBe(result);
});

test('should throw error if you try to rewrite readable properties', () => {
  const target = Object.defineProperty({}, 'prop', {
    value: 'Ivan',
    writable: false
  });

  expect(() => Object.assign(target, { k: 'v' }, { prop: 'n' })).toThrow();
  // END
});
