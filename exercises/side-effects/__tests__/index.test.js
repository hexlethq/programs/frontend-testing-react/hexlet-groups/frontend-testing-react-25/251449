const fs = require('fs');
const path = require('path');
const { upVersion } = require('../src/index.js');

// BEGIN
let dirPath = './__fixtures__/package.json';

test('should correct update patch version', () => {
  upVersion(dirPath);

  let fileContent = fs.readFileSync(dirPath, 'utf8');

  expect(JSON.parse(fileContent)).toStrictEqual({
    version: '1.3.3'
  })
});

test('should correct update minor version', () => {
  upVersion(dirPath, 'minor');

  let fileContent = fs.readFileSync(dirPath, 'utf8');

  expect(JSON.parse(fileContent)).toStrictEqual({
    version: '1.4.0'
  })
});

test('should correct update major version', () => {
  upVersion(dirPath, 'major');

  let fileContent = fs.readFileSync(dirPath, 'utf8');

  expect(JSON.parse(fileContent)).toStrictEqual({
    version: '2.0.0'
  });
});





// END
