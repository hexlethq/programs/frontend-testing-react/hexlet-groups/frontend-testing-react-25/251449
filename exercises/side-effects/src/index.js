const fs = require('fs');

// BEGIN
const versionPartPos = {
  'major': 0,
  'minor': 1,
  'patch': 2
}

function upVersion(dirPath, versionPart = 'patch') {
  const currentPosition = versionPartPos[versionPart];

  let fileContent;
  try {
    fileContent = fs.readFileSync(dirPath, 'utf8');
  } catch (error) {
    throw new Error(`Can't read file ${dirPath}`);
  }

  const content = JSON.parse(fileContent);    // { version: '1.3.2' }
  const curVersion = content.version;         // 1.3.2
  let versionArr = curVersion.split('.');     // ['1', '3', '2']

  versionArr[currentPosition] = parseInt(versionArr[currentPosition]) + 1;
 
  if (currentPosition !== 2) {
    for(let i = currentPosition + 1; i < versionArr.length; i++) {
      versionArr[i] = 0;
    }
  }

  const updateVersion = {...content, version: versionArr.join('.')};

  try {
    fs.writeFileSync(dirPath, JSON.stringify(updateVersion), 'utf8');
  } catch (error) {
    throw new Error(`Can't write to file ${dirPath}`);
  }
}
// END

module.exports = { upVersion };
