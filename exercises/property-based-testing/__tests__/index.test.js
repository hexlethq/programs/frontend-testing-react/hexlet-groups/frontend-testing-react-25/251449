const fc = require('fast-check');

const sort = (data) => data.slice().sort((a, b) => a - b);

// BEGIN
test('should do correct sorting for integer cases', () => {
  fc.assert(
    fc.property(
      fc.array(fc.integer()), data => {
        const sorted = sort(data);
        expect(sorted.length).toBe(data.length);
        expect(sorted).toBeSorted({ descending: false });
      }
    )
  )
});

test('should do correct sorting for float cases', () => {
  fc.assert(
    fc.property(
      fc.array(fc.float()), data => {
        const sorted = sort(data);
        expect(sorted.length).toBe(data.length);
        expect(sorted).toBeSorted({ descending: false });
      }
    )
  )
});
// END
